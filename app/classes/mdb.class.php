<?php
class mdb{

	private static $mdb;

	public static function getInstance(){
		if(!isset(self::$mdb)){
			// Получаем конфигурацию mongoDB
			$config = Spyc::YAMLLoad(app_path()."/config/yml_config/mongo.yml");

			// Пытаемся получить подключение к mongo
			try{
				self::$mdb = new MongoClient("mongodb://{$config['user']}:{$config['password']}@{$config['server']}:{$config['port']}/{$config['db']}");
			}catch (Exception $e){

				ChromePhp::log($e);
			}

		}

		return self::$mdb;
	}

	private function __construct(){}

}
