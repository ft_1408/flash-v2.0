<?php

use PhpAmqpLib\Connection\AMQPConnection;
use PhpAmqpLib\Message\AMQPMessage;

class amqp{

    // Конфигурация сервера
    private $host;
    private $port;
    private $user;
    private $password;
    private $vhost;

    // Канал и соединение
    private $connection;
    private $channel;

    // Конфигурация очереди и маршрутизации
    private $exchange;
    private $routing_key;

    // Конструктор класса
    public function __construct($host, $port, $user, $password, $vhost){
        // Инициализация переменных класса
        $this->host = $host;
        $this->port = $port;
        $this->user = $user;
        $this->password = $password;
        $this->vhost = $vhost;

        try{
            // Создаем подключение к ребиту
            $this->connection = new AMQPConnection( $this->host,
                                                    $this->port,
                                                    $this->user,
                                                    $this->password,
                                                    $this->vhost);

        }catch(Exception $e){
            ChromePhp::log($e->getMessage());
        }
    }

    public function setQueue($queue_config){

        // Сохраняем настройки очереди
        $this->exchange   = $queue_config['exchange'];
        $this->routing_key = $queue_config['routing_key'];

        // Создадим канал
        $this->channel = $this->connection->channel();
        // Создадим exchange
        $this->channel->exchange_declare($queue_config['exchange'], $queue_config['exchange_type'], false, false, false);
        // Создадим очередь
        $this->channel->queue_declare($queue_config['name'], false, false, false, false);
        // Создадим бинд обменника и очереди по ключу
        $this->channel->queue_bind($queue_config['name'], $queue_config['exchange'], $queue_config['routing_key']);
    }

    public function sendMessage($msg){
        if (! isset($this->channel)){
            throw new Exception("Queue not decladed", 1);

        }

        // Превращаем массив - соощение в строку
        $content = serialize($msg);

        // Получаем AMQP - сообщение
        $msg = new AMQPMessage($content);


        // Отправляем сообщение
        $this->channel->basic_publish($msg, $this->exchange, $this->routing_key);

    }

    public function __destruct(){
        $this->channel->close();
        $this->connection->close();
    }
}
