<?php
class FileController extends Controller {
	public function postList(){
		$files = array();

		foreach (MongoFile::getCurrentDirFileList() as $value) {
			// Получим человеко-читаемый размер файла
			$human_fs = MongoFile::convertFilesize($value['file_size']);

			// Добавим его в массив с данными ответа
			$value['human_readable_fs'] = $human_fs;
			$files[] = $value;
		}

		return json_encode($files);
	}

	public function postDelete(){
		$fid = Input::get('id');

		$result = MongoFile::deleteFile($fid);

		return json_encode($result);
	}

	public function postRename(){
		$fid = Input::get('id');
		$name = Input::get('name');

		$result = MongoFile::renameFile($fid,$name);

		return json_encode($result);
	}
}
