<?php

class FileuploadController extends BaseController {

	public function uploadFile(){
		error_reporting(E_ALL | E_STRICT);

		// Парсим конфиг из yml - файла настроек для amqp
		$amqp_config = Spyc::YAMLLoad(app_path()."/config/yml_config/amqp.yml");

		// Создадим массив с мета информацией
		$meta = array();

		// Получим ID текущего пользователя
		$meta['user_id'] = Auth::user()->id;

		// Получим e-mail текущего пользователя
		$meta['user_email'] = Auth::user()->email;

		// Получим директорию в которой в данный момент находиться пользователь
		$meta['user_dir'] = User::getCurrentDir();

		// Сформируем массив с конфигурацией
		$settings = array(
			'script_url' => __DIR__ . '/FileuploadController.php',
			'upload_dir' => storage_path() . "/files/",
			'upload_url' => storage_path() . "/files/",
			'amqp' => $amqp_config,
			'meta' => $meta
		);


		// Создадим класс приема файлов
		$handler = new Components\RabbitUpload($settings);

	}

}
