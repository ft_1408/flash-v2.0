<?php

// restful-controller
class UserController extends BaseController {

    public function __construct(){
        // Обьявим фильтры
        $this->beforeFilter('guest');

    }

	// Если пришли get-oм
    public function getIndex(){

        // Определяем все сегменты текущего маршрута
        $segm = Request::segments();
        // Берем последний если не найден то устанавливаем его в "home"
        $segm = end($segm) ? end($segm) : 'home';

        // Выбераем View на основе пришедшего запроса
        $this->layout->content = View::make("{$segm}.index");
        $this->layout->template = "{$segm}-template";
    }

    // Если пришли post-оm
    public function postAdd(){
    	// Получим данные, введенные пользователем
    	$user = Input::all();

        if(!$user['email'] || !$user['password'] || !$user['name']){
            return Redirect::to('/adduser')->with('message', 'Заполненны не все поля.');
        }

        // Проверяем есть ли уже пользователь с таким e-mail
        $isuser = DB::table('users')->where('email', $user['email'])->first();

        // Если есть - отправим сообщение об ошибке
        if($isuser){
            return Redirect::to('/adduser')->with('message', 'Пользователь с таким e-mail уже существует.');
        }

    	// Получим IDшник
    	$id = sha1(uniqid().$user['name']);

    	$currentTime = time();

    	// Создадим соль для хеша пароля
    	$salt = sha1($currentTime . uniqid(rand(),1) . $id);

    	// Создаем модель пользователя
    	$newUser =  new User;

    	// Заполняем ёё полями
    	$newUser->id = $id;
    	$newUser->email = $user['email'];
    	$newUser->password = Hash::make($salt . $user['password']);
    	$newUser->name = $user['name'];
    	$newUser->salt = $salt;

    	// Сохраняем
    	$result = $newUser->save();

    	return Redirect::to('/login')->with('success', 'Вы успешно зарегестрированны.
            Пожалуйста войдите со своим логином и паролем');
    }
}
