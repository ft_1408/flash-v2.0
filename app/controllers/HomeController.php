<?php

class HomeController extends BaseController {

	public function showWelcome(){

		// Определяем все сегменты текущего маршрута
		$segm = Request::segments();
		// Берем последний если не найден то устанавливаем его в "home"
		$segm = end($segm) ? end($segm) : 'home';

		// Выбераем View на основе пришедшего запроса
		$this->layout->content = View::make("{$segm}.index");
    	$this->layout->template = "{$segm}-template";
	}
}
