<?php
class FolderController extends Controller {

	public function postList(){
		$folders = Folder::getCurrentUserFolders();
		return json_encode($folders);
	}

	// Удаление папки
	public function postDelete(){
		// Получаем id папки, которую нужно удалить
		$fid = Input::get('fid');

		// Удаляем данную папку у текущего пользователя
		$result = Folder::delete($fid);

		return json_encode($result);
	}

	// Добавление папки
	public function postAdd(){
		// Получаем id папки, которую нужно удалить
		$folderName = Input::get('name');

		// Добавляем папку текущему пользователю
		$result = Folder::add($folderName);

		return json_encode($result);
	}

	// Смена директории
	public function getChange(){
		// Получим ID директории в которую нужно переместиться
		$fid = Input::get('fid');

		// Если ID равен 0 то это домашняя директория - перемещаемся в нее
		if($fid == 0){
			// Переместимся в данную директорию
			Folder::setCurrentDir('/');

		}else{
			// Получим инфомацию о папке
			$folder = Folder::getFolderById($fid);

			// Переместимся в данную директорию
			Folder::setCurrentDir($folder['dir']);

		}

		// Переадресуем пользователя на домашнюю страницу
		return Redirect::to('/');

	}

	// Получить информацию о всех директориях текущего пути
	public function postPath(){
		// Получим информацию о всех директориях текушего пути
		$result = Folder::getFoldersByCurrentPath();

		return json_encode($result);
	}

	public function postRename(){
		$fid = Input::get('id');
		$name = Input::get('name');

		$result = Folder::rename($fid, $name);

		return json_encode($result);
	}
}
