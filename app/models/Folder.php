<?php
	class Folder{

	// Метод получения текущей директории ползователя
	public static function getCurrentDir(){

		// Если в сессии не найдена текущая директория пользователя
		// установим её по умолчанию
		if (!Session::has('current_user_dir')) {
			Session::put('current_user_dir', '/');
		}

		// Получаем текущюю директорию пользователя
		$result = Session::get('current_user_dir');

		return $result;
	}

	// Метод для установки текущей директории пользователя
	// по умолчанию директория "/"
	public static function setCurrentDir($dir = '/'){

		// Установим текущей директорией переданную
		$result = Session::put('current_user_dir', $dir);

		return $result;
	}


	// Метод создает новую папку в текущей директории
	// Принемает имя папки $folderName
	public static function add($folderName){
		// Получаем необходимую информацию по текущему пользователю
		$uid = Auth::user()->id;
		$currDir = Folder::getCurrentDir();

		// Получаем коннект к mongoDB
		$mdb = mdb::getInstance();

		$collection = $mdb->selectCollection("flash","user.folders");

		$dirPath = $currDir. $folderName . '/';

		// Вставляемый в БД объект
		$dirInfo = new StdClass();

		// Информация о текущей директории
		$dirInfo->uid = $uid;
		$dirInfo->dir = $dirPath;
		$dirInfo->originDir = $currDir;
		$dirInfo->folderName = $folderName;

		$collection->insert($dirInfo);

	}

	// Получает папки пользователя по его ID
	// Принемает $uid ID пользователя
	// Возвращает array - массив с информацией о папках пользователя
	private static function getUserFolders($uid){
		// Получаем коннект к mongoDB
		$mdb = mdb::getInstance();
		$currDir = Folder::getCurrentDir();

		$collection = $mdb->selectCollection("flash","user.folders");

		$folders = $collection->find(array('uid' => $uid, 'originDir' => $currDir))->sort(array('folderName' => 1));

		$result = array();

		foreach ($folders as $folder) {
			$result[] = $folder;
		}

		return $result;
	}

	// Получает список папок текущего пользователя
	public static function getCurrentUserFolders(){
		$uid = Auth::user()->id;

		$result = self::getUserFolders($uid);

		return $result;
	}

	// Удаляет папку по ёё ID
	public static function delete($fid){
		$uid = Auth::user()->id;

		$mdb = mdb::getInstance();

		$mId = new MongoId($fid);

		$collection = $mdb->selectCollection("flash","user.folders");
		$collectionFiles = $mdb->selectCollection("flash","user.files");

		// Получаем удаляемую папку
		$removedFolder = $collection->findOne(array('uid'=>$uid, '_id'=>$mId));

		// Получаем все подпапки
		$subfolders = $collection->find(array('uid'=>$uid, 'originDir'=>$removedFolder['dir']));

		// Получаем файлы в них
		$files = $collectionFiles->find(array('user_id'=>$uid, 'user_file_dir'=>$removedFolder['dir']));

		// Проходимся по субдиректориям
		foreach($subfolders as $f){
			// Удаляем все файлы из каждой из них
			$file = $collectionFiles->remove(array('user_id'=>$uid, 'user_file_dir'=>$f['dir']));
		}

		// Удаляем субдиректории
	 	$collection->remove(array('uid'=>$uid, 'originDir'=>$removedFolder['dir']));
	 	// Удаляем саму папку
		$collection->remove(array('uid'=>$uid, '_id'=>$mId));
	}

	// Получает информацию о папке по ее ID
	public static function getFolderById($fid){
		$uid = Auth::user()->id;

		$mdb = mdb::getInstance();

		$mId = new MongoId($fid);

		$collection = $mdb->selectCollection("flash","user.folders");

		$result = $collection->findOne(array('uid'=>$uid, '_id'=>$mId));

		return $result;
	}

	// Получить директорию по ее имени
	private static function getFolderByName($name){
		$uid = Auth::user()->id;

		$mdb = mdb::getInstance();

		$collection = $mdb->selectCollection("flash","user.folders");

		$result = $collection->findOne(array('uid'=>$uid, 'folderName'=>$name));

		return $result;
	}

	// Получить информацию о всех директориях составляющих текущий путь пользователя
	public static function getFoldersByCurrentPath(){
			$result = array();

			// Получаем путь к текущему диалогу пользователя
			$folderPath = Folder::getCurrentDir();

			// Получаем массив директорий
			$folderPath = explode('/', $folderPath);


			// Удаляем последний елемент и корневую папку - они не нужны
			unset($folderPath[count($folderPath) - 1]);
			unset($folderPath[0]);
			// Получим информацию по каждой директории
			foreach ($folderPath as $name) {
				$result[] = self::getFolderByName($name);
			}
			return $result;
	}

	public static function rename($id, $name){
		// Получаем коннект к mongoDB
		$mdb = mdb::getInstance();
		$currDir = Folder::getCurrentDir();
		$uid = Auth::user()->id;

		$mId = new MongoId($id);

		// Обновленная директория
		$newDir = $currDir. $name . '/';

		$collection = $mdb->selectCollection("flash","user.folders");

		$res = $collection->update(array('uid' => $uid, '_id' => $mId, 'originDir' => $currDir),
								array(
									'$set' => array("folderName" => "$name", "dir"=>$newDir)));
	}
}
