<?php
class MongoFile {

	// mongoDB connection
	private $mdb;

	// Constructor
	public function __construct($mdb){

		// Get mdb connection
		$this->mdb = $mdb;
	}

	/*
	* @return mongoCursor $files - list of files current user in current
	* directory
	*/
	public static function getCurrentDirFileList(){
		// Getting user info
		// user ID
		$uid = Auth::user()->id;

		// User Current Dir
		$dir = Folder::getCurrentDir();

		// mdb connection
		$mdb = mdb::getInstance();

		$collection = $mdb->selectCollection("flash","user.files");

		$files = $collection->find(array('user_file_dir' => $dir,
						  				 'user_id' => $uid));

		return $files;
	}

	private static function getFileById($id){
		// Получаем коннект к mongoDB
		$mdb = mdb::getInstance();
		$currDir = Folder::getCurrentDir();
		$uid = Auth::user()->id;

		$mId = new MongoId($id);

		$collection = $mdb->selectCollection("flash","user.files");

		$file = $collection->findOne(array('user_id' => $uid, '_id' => $mId, 'user_file_dir' => $currDir));

		return $file;
	}

	// Удаление записи о файле из БД
	private static function removeById($id){
		// Получаем коннект к mongoDB
		$mdb = mdb::getInstance();
		$currDir = Folder::getCurrentDir();
		$uid = Auth::user()->id;

		$mId = new MongoId($id);

		$collection = $mdb->selectCollection("flash","user.files");

		$result = $collection->remove(array('user_id' => $uid, '_id' => $mId, 'user_file_dir' => $currDir));

		return $result;
	}

	public static function renameFile($id, $name){
		// Получаем коннект к mongoDB
		$mdb = mdb::getInstance();
		$currDir = Folder::getCurrentDir();
		$uid = Auth::user()->id;

		$mId = new MongoId($id);

		$collection = $mdb->selectCollection("flash","user.files");

		$res = $collection->update(array('user_id' => $uid, '_id' => $mId, 'user_file_dir' => $currDir),
								array(
									'$set' => array("user_fileName" => "$name")));
	}

	// Функция управляет удалением файла
	public static function deleteFile($id){

		$msg = array();
		$config = Spyc::YAMLLoad(app_path()."/config/yml_config/amqp.yml");

		// Получаем конфигурацию очереди и сервера
		$srv_config = $config['srv'];
		$queue_config = $config['remove'];

		// Пытаемся получить файл
		$file = self::getFileById($id);

		// Если не получилось получить файл выбрасываем Exeption
		if(!$file){
			throw new Exception("File not exists", 1);
		}

		// Устанавливаем подключение с message-broker-ом
		$amqp = new amqp($srv_config['host'],
						 $srv_config['port'],
						 $srv_config['user'],
						 $srv_config['password'],
						 $srv_config['vhost']);


		// !!ВАЖНО!! переопределяем название очереди в соответствии с названием хоста (может не работать)
		$queue_config['name'] = $file['host'];
		$queue_config['routing_key'] = $file['host'] . '_delete';

		// Устанавливаем параметры очереди и обменника
		$amqp->setQueue($queue_config);

		// Формируем сообщение удаления файла
		$msg['imachine_fileName'] = $file['machine_fileName'];
		$msg['id'] = $file['_id'];
		$msg['uid'] = $file['user_id'];

		// Отправляем сообщение
		$amqp->sendMessage($msg);

		// Удаляем запись о файле из БД
		self::removeById($id);

		return true;
	}

	// Конфертирует размер файла в байтах в человекочитаемый
	// формат
	public static function convertFilesize($file_size){
		// Форматы файлов
		$type = array("", "K", "M", "G", "T", "P", "Ex", "Z", "Y");
		// Переменная хранит индекс формата
		$index = 0;

		// Получаем максимальный индекс формата, в который можем перевести
		while($file_size >= 1024) {
		    $file_size /= 1024;
		    $index++;
		}
		// Округляем полученный размер до двух знаков
		$file_size = round($file_size, 2);

		// Возвращаем округленный размер файла
		return($file_size." ".$type[$index]."B");
	}
}

