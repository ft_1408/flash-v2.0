<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	// public $timestamps = false;

	/**
	 * The primary key of the table.
	 *
	 * @var string
	 */
	protected $primaryKey = 'id';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');



	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

	// Метод получения текущей директории ползователя
	public static function getCurrentDir(){

		// Если в сессии не найдена текущая директория пользователя
		// установим её по умолчанию
		if (!Session::has('current_user_dir')) {
			Session::put('current_user_dir', '/');
		}

		// Получаем текущюю директорию пользователя
		$result = Session::get('current_user_dir');

		return $result;
	}

	// Метод для установки текущей директории пользователя
	// по умолчанию директория "/"
	public static function setCurrentDir($dir = '/'){

		// Установим текущей директорией переданную
		$result = Session::put('current_user_dir', $dir);

		return $result;
	}

	public function getRememberToken()
	{
	    return $this->remember_token;
	}

	public function setRememberToken($value)
	{
	    $this->remember_token = $value;
	}

	public function getRememberTokenName()
	{
	    return 'remember_token';
	}
}
