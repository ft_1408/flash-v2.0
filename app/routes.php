<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// Определяем все сегменты текущего маршрута
$segm = Request::segments();
// Берем последний если не найден то устанавливаем его в "home"
$segm = end($segm) ? end($segm) : 'home';


// Объявление REST-контроллера для добавления нового пользователя
Route::controller('adduser', 'UserController');


// Фильтр отбросит всех неавторизированных пользователей на страницу логина
Route::group(array('before' => 'auth'), function () {

	// restfull контроллер для операций с папками
	Route::controller('folder', 'FolderController');

	// restfull контроллер для операций с файлами
	Route::controller('file', 'FileController');

	// Маршрут на главную страницу - биндим фильтр "auth"
	Route::get('/', array('uses'=>'HomeController@showWelcome'));

	Route::post('/', array('uses'=>'FileuploadController@uploadFile'));

	// Маршрут на главную страницу - биндим фильтр "auth"
	Route::get('/logout', array('before' => 'auth', function(){

		Auth::logout();
		return Redirect::to('/login');

	}));
});

// Фильтр отбросит всех залогиненных пользователей на главную страницу
Route::group(array('before' => 'guest'), function () {

	// Страничка логина
	Route::get('/login', array('uses'=>'LoginController@login'));

	// Если на страничку логина попали постом
	Route::post('/login', array('before' => 'csrf', function(){
		// Получаем массив с переданными данными
		$authData = Input::all();

		// Пытаемся получить пользователя по email
		$user = User::where('email', '=', $authData['uname'])->first();
		if($user){
			// Если пользователь найден получаем соль для пароля
			$salt = $user->salt;


			$credentials = array('email' => $authData['uname'],
								 'password' => $salt . $authData['passwd']);

			$stay_loggined = (bool) array_key_exists('stay_loggined', $authData);

			// Пытаемся авторизироваться
			if (Auth::attempt($credentials, $stay_loggined)){

		  		return Redirect::to('/');
			}else{
				return Redirect::to('/login')->with('error', 'Не верный пользователь или пароль.');
			}
		}else{

			// TODO: перенести весь функционал в контроллер

			return Redirect::to('/login')->with('error', 'Не верный пользователь или пароль.
				Пожалуйста, <a href="/adduser">зарегестрируйтесь</a>');
		}
	}));
});




