<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	  	Schema::create('users', function ($table) {
		    $table->increments('id');
		    $table->string('email')->unique();
		    $table->string('password');
		    $table->string('name');
		    $table->string('salt');
		    $table->string('nullable');
		    $table->text('remember_token');
		    $table->timestamps();
	  	});
	}
	public function down()
	{
		Schema::drop('users');
	}

}
