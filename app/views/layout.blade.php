<!doctype html>
<html>

	<head>
	<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>flash</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="css/libs/plusstrap/plusstrap.min.css">
        <link rel="stylesheet" type="text/css" href="css/libs/plusstrap/plusstrap-responsive.min.css">

		@yield('extraCss')

		<script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
		<script src="js/libs/jquery.ui/jquery.ui.widget.js"></script>
		<script src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
		<script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>

		<script src="js/libs/jquery.fileupload/jquery.iframe-transport.js"></script>
		<script src="js/libs/jquery.fileupload/jquery.fileupload.js"></script>
		<script src="js/libs/jquery.fileupload/jquery.fileupload-process.js"></script>
		<script src="js/libs/jquery.fileupload/jquery.fileupload-audio.js"></script>
		<script src="js/libs/jquery.fileupload/jquery.fileupload-image.js"></script>
		<script src="js/libs/jquery.fileupload/jquery.fileupload-video.js"></script>
		<script src="js/libs/jquery.fileupload/jquery.fileupload-validate.js"></script>

		<script src="js/libs/plusstrap/bootstrap.js"></script>

		<script src="js/common/utils.js"></script>
		<script src="js/common/registry.js"></script>

	</head>

	<body>
		<div class="wrapper">
			@yield('extraScripts')
	    	@yield('content')
    	</div>
  	</body>
</html>
