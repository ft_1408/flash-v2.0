<div id="addFilesHolder" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Добавление файлов</h3>
	</div>

  	<div class="modal-body">
		<div id="upload_container_dialog">
			<div class = 'addFile'>
				<input id="fileupload" type="file" name="files[]" multiple>
				<button type="button" class='btn btn-primary add-files'>Добавить файлы</button>
				<button type="button" class='btn btn-primary add-files-all' style='display:none' >Загрузить всё</button>
			</div>

<!-- 		    <div id="progress" class="progress">
		        <div class="progress-bar progress-bar-success"></div>
		    </div> -->
		</div>
	</div>
	<!-- <div class="modal-footer">
		<a href="#" class="btn" id="cancel" >Отмена</a>
		<a href="#" class="btn btn-primary" id="ok" >Ок</a>
	</div> -->
</div>
