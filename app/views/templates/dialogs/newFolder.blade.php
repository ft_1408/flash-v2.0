
<div id="newFolderDialog" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3>Cоздание новой папки</h3>
	</div>

  	<div class="modal-body">
		<input type="text" id='folderName' placeholder="Введите название папки">
		</div>
		<div class="modal-footer">
			<a href="#" class="btn" id="cancel" >Отмена</a>
			<a href="#" class="btn btn-primary" id="ok" >Ок</a>
		</div>
</div>
