@section('extraCss')
{{HTML::style("css/login.css")}}
@stop

@section('extraScripts')
{{HTML::script("js/login.js")}}
@stop

@section('content')
	<main>
		<img src="css/images/server.gif">
		{{ Form::open(array('url' => '/login')) }}
			<div class='enter-label-container'>
				<div class="enter">
					{{ Form::label('enter', 'Вход') }}
				</div>
				<div class='register'>
					или <a href="/adduser">регистрация</a>
				</div>
			</div>

			<div class="uname">
				{{ Form::label('uname', 'Адрес електронной почты') }}
				{{ Form::text('uname', null, array('autocomplete'=>'off')) }}
			</div>

			<div class="passwd">
				{{ Form::label('passwd', 'Пароль') }}
				{{ Form::password('passwd', null, array('autocomplete'=>'off')) }}
			</div>

			<div class="stay_loggined">
				{{ Form::checkbox('stay_loggined') }}
				{{ Form::label('stay_loggined', 'Оставаться в системе') }}
			</div>

			<div class="button">
				{{ Form::submit('Войти', array('disabled'=>'disabled')) }}
			</div>
			<div class='messages'>
				@if(Session::get('error'))
				<div class='error'>{{Session::get('error')}}</div>
				@endif

				@if(Session::get('success'))
				<div class='success'>{{Session::get('success')}}</div>
				@endif
			</div>
		</main>
	{{ Form::close() }}
@stop
