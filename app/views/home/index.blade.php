
@section('extraCss')
{{HTML::style("css/home.css")}}
@stop

@section('extraScripts')
{{HTML::script("js/widgets/fileUploadWidget.js")}}
{{HTML::script("js/widgets/FileListWidget.js")}}
{{HTML::script("js/widgets/NavBarWidget.js")}}
{{HTML::script("js/widgets/helpers/ChangeDirectoryHelper.js")}}
{{HTML::script("js/widgets/UserActionWidget.js")}}
{{HTML::script("js/home.js")}}
@stop

@section('content')
	{{-- Подключаем html-шаблон для диалогового окна удаления папки --}}
	@include('templates.dialogs.dellFolder')

	{{-- Подключаем html-шаблон для диалогового окна добавления папки --}}
	@include('templates.dialogs.newFolder')

	{{-- Подключаем html-шаблон для диалогового окна добавления файлов --}}
	@include('templates.dialogs.addFiles')

	{{-- Подключаем html-шаблон для диалогового окна добавления папки --}}
	@include('templates.menu.dropdown')

	<nav id='header'>
		{{-- Подключаем html-шаблон для виджета управления директориями --}}
		@include('templates.helpers.changeDirectoryHelper')
		<a id="dLabel" class="dropdown-toggle user" data-toggle="dropdown" data-target="#">{{Auth::user()->name}}</a>
		<ul class="dropdown-menu user-dropdown-menu" role="menu" aria-labelledby="dLabel">
			<!-- <li id="createFolder">Создать папку</li> -->
			<li id="logout">Выход</li>
		</ul>
	</nav>

	<div id='user_actions'>
		<ul>
			<li><button type="button" class='btn btn-primary' id='add-files'>Добавить файлы</button></li>
			<li><button type="button" class='btn btn-primary' id='add-folder'>Создать папку</button></li>
		</ul>
	</div>

	<div id="upload_container">
		<div class = 'addFile'>
			<input id="fileupload" type="file" name="files[]" multiple>
			<!-- <button type="button" class='btn btn-primary add-files-all' style='display:none' >Загрузить всё</button> -->
			<!-- <button type="button" class='btn btn-primary add-files'>Добавить файлы</button> -->
		</div>
<!--
	    <div id="progress" class="progress">
	        <div class="progress-bar progress-bar-success"></div>
	    </div> -->
	</div>
	<main>
		<table id="files" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>Тип</th>
					<th>Название</th>
					<th>Вес</th>
					<th></th>
				</tr>
			</thead>
			<tbody></tbody>
		</table>
	</main>
@stop
