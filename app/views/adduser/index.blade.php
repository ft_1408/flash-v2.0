@section('extraCss')
{{HTML::style("css/adduser.css")}}
@stop

@section('extraScripts')
{{HTML::script("js/addUser.js")}}
{{HTML::script("js/widgets/Form_Widget.js")}}
@stop


@section('content')
    <main>
        <img src="css/images/add_user.png">
    	{{ Form::open(array ('url' => '/adduser/add')) }}
            <div class="enter">
                {{ Form::label('enter', 'Добавление нового пользователя') }}
            </div>
            <div class='e-mail'>
                {{ Form::label('email', 'e-mail') }}
                {{ Form::text('email', null, array('autocomplete'=>'off', 'class'=>'required' )) }}
            </div>

            <div class='password'>
                {{ Form::label('password', 'Пароль') }}
                {{ Form::password('password', null, array('autocomplete'=>'off')) }}
            </div>

            <div class='name'>
        		{{ Form::label('name', 'Имя') }}
        		{{ Form::text('name', null, array('autocomplete'=>'off', 'class'=>'required' )) }}
            </div>

            <div class="button">
    		    {{ Form::submit('Ок', array('disabled'=>'disabled')) }}
            </div>
    	{{ Form::close() }}

        <div class='messages'>
            @if(Session::get('message'))
            <div>{{Session::get('message')}}</div>
            @endif
        </div>
    </main>
@stop
