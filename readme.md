# Requiroments
- php 5.4 - 5.6
- composer
- rabbitmq server
- php-bcmath
- php-xsl
- php-xml
- php-mcrypt
- mongodb
- mysql

# Notes
Before first install remove all pre post scripts from composer.json after sucessfull instalation turn it back
Then fix one million dependencies

### Exsample of vhost file (apache)
    <VirtualHost *:80>
        ServerName flash.local.com

        ServerAdmin webmaster@localhost
        DocumentRoot /flash_data/flash-v2.0/public

        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
        <Directory /flash_data/flash-v2.0/public>
                 DirectoryIndex index.php
                AllowOverride All
                Require all granted
                Order allow,deny
                Allow from all
        </Directory>
    </VirtualHost>
