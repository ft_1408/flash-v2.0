function ChangeDirectoryHelper($selector, $parent){

    var $this = this;

    $this.parent = parent;

    $this.init = function(){
        $this.widget = $($selector);

        // Получаем текущий путь директорий
        $this.getCurrentDirPath();
    }

    $this.getCurrentDirPath = function(){
        $.ajax({
              type: "POST",
              url: "/folder/path",
              success: function(data){
                var $container = $this.widget.find('.container');

                // Домашний каталог в зависимости от того находимся ли мы в какой то папке
                $home = (data.length > 0) ? "href='/folder/change?fid=0'" : "";
                // Начнем формировать навигацию
                $container.append("<a "+$home+">home</a><span>/</span>");

                if(!$.isEmptyObject(data)){
                    // Добавим каждую папку в навигатор
                    $.each(data, function($i, $v){
                        // Получим ID папки
                        $fid = $v._id.$id;

                        // Добавим елемент в хелпер навигации
                        $container.append("<a href='/folder/change?fid="+$fid+"'>"+$v.folderName+"</a><span>/</span>");
                    });
                }
              },
              dataType: "json"
            });
    }

    $this.init();

}
