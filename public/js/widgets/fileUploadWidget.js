function fileUploadWidget($selector, $parent){
	var $this = this;

    $this.parent = $parent;

	$this.widget = {};

    $this.count = 0;

	$this.init = function(){
		$this.widget = $($selector);

		$this.render();

	    $this.initEventListeners();
	}

	$this.initEventListeners = function(){
		$this.widget.find('#fileupload')
		  .on('fileuploadadd', $this.onFileUploadAdd)
		  .on('fileuploadprocessalways', $this.onFileuploadProcessAlways)
		  .on('fileuploadprogressall', $this.onFileuploadProgressAll)
		  .on('fileuploaddone', $this.onFileuploadDone)
		  .on('fileuploadfail',$this.onFileuploadFail);
	}

	$this.render = function(){

		$this.uploadButton = $('<button/>')
	            .addClass('btn btn-primary')
	            .prop('disabled', true)
	            .text('Processing...')
	            .on('click', $this.fileButtonClick);


	    $this.widget.find('#fileupload').fileupload({
	        url: "/",
	        dataType: 'json',
	        autoUpload: false,
	        maxFileSize: 400 * 1024 * 1024, // 400 MB
	        disableImageResize: /Android(?!.*Chrome)|Opera/
	            .test(window.navigator.userAgent),
	        previewMaxWidth: 100,
	        previewMaxHeight: 100,
	        previewCrop: true
	    })
		  .prop('disabled', !$.support.fileInput)
	        .parent().addClass($.support.fileInput ? undefined : 'disabled');
	}

	$this.fileButtonClick = function(){
		var $button = $(this),
            $data = $button.data();

            $button.off('click').text('Abort').on('click', function () {
                $button.remove();
                $data.abort();
            });

            $data.submit().always(function () {
                $button.remove();

                // Обновить список файлов пользователя после того как файл загружен на сервер
                $this.parent.components.FileListWidget.updateFoldersList();
                // $this.widget.trigger('uploadDone',[]);
            });
	}

	$this.onFileUploadAdd = function (e, data) {

        data.context = $('<div/>').appendTo('#files');
        $.each(data.files, function (index, file) {
            var node = $('<div/>').addClass('filePrev')
                    .append($('<span/>').text(file.name));
            if (!index) {
                node
                    .append('<br>')
                    .append($this.uploadButton.clone(true).data(data).addClass('uplFilePrev'));
            }
            node.appendTo($this.widget);
            $this.count ++;
    	});

        //  Если пришло больше 1 файла
        if($this.count > 1){
            var $uploadAll = $this.widget.find('.add-files-all');
            if(!$uploadAll.is(":visible")){
                $uploadAll.toggle().on('click', $this.submitAll);
            }

        }
    };

    $this.onFileuploadProcessAlways = function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));

            //  При ошибке блокируем кнопку загрузки файлов
            $this.widget.find('.add-files').prop('disabled', 'true');
        }

        // Добавление кнопок загрузки файлов
        if (index + 1 === data.files.length) {
            $this.widget.find('button').not('.add-files')
                .text('Загрузить')
                .prop('disabled', !!data.files.error);
        }
    }

    // Создание прогрессбара
    $this.onFileuploadProgressAll = function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }

    $this.onFileuploadDone =  function (e, data){
    	$.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);

            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }

    $this.onFileuploadFail = function (e, data) {
    }

    $this.submitAll = function(){
        $this.widget.find('.filePrev').each(function($i, $v){
            $($v).find('.uplFilePrev').trigger('click');
        })
    }

	$this.init();
}
