function Form_Widget($selector, $parent){
	var $this = this
	$this.selector = $selector;

	$this.init = function(){
		$this.widget = $($this.selector);

		$this.widget.find('input').on('focus', $this.onFocus);
		$this.widget.find('input').on('input', $this.onKeydown);

	}

	$this.onFocus = function($e){
		$this.widget.find('.focused').removeClass('focused');

		$label = $($e.target).parent().find('label');
		$label.addClass('focused');
	}

	$this.onKeydown = function($e){
		$input = $($e.target);

		// Плейсхолдер для инпута
		$label = $($e.target).parent().find('label');

		if($input.val()){
			$label.css('display','none');

			if($this.checkFields()){
				$this.unblockButton();
			}
		}else{
			$label.css('display','inline');
			$this.blockButton();
		}
	}

	// Функция блокирует кнопку отправки формы
	$this.blockButton = function(){
		$this.widget.find('.button input').attr('disabled', 'true');
	}

	// Функция разблокирует функцию отправки формы
	$this.unblockButton = function(){
		$this.widget.find('.button input').removeAttr('disabled');
	}

	// Функция проверяет введенны ли в поля значения
	$this.checkFields = function(){
		var $result = true;

		$this.widget.find('.required').each(function($i, $e){
			$result = $result && !!$($e).val()
		})

		return $result;
	}

	$this.init();
}
