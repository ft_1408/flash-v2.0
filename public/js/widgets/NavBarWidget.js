function NavBarWidget($selector, $parent){
    var $this = this;
    $this.parent = $parent;

    $this.init = function(){
        $this.widget = $($selector);

        $this.userDropdown = $this.widget.find('.user-dropdown-menu');
        $this.userDropdown.find("#logout").on('click', $this.onLogout);
        $this.userDropdown.find("#createFolder").on('click', $this.onCreateFolderClick);
    }

    $this.onLogout = function(){

        window.location.href = "/logout";
    }

    $this.onCreateFolderClick = function(){
        $this.parent.components.FileListWidget.onAddNewFolder();
    }

    $this.init();
}
