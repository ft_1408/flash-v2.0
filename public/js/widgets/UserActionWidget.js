function UserActionWidget($selector, $parent){
    var $this = this;
    $this.components = {};
    $this.parent = $parent;
    $this.selector = $selector;
    $this.process = {};

    $this.init = function(){
    	$this.widget = $($this.selector);

    	$this.addFileButton = $this.widget.find('#add-files');
    	$this.addFolderButton = $this.widget.find('#add-folder');
    	$this.fileContainer = $this.widget.find('#upload_container_dialog');

    	$this.components.uploadWidget = new fileUploadWidget('#upload_container_dialog', $this.parent);

    	$this.addFileButton.on('click', $this.onAddFile);
    	$this.addFolderButton.on('click', $this.onAddFolder);

    	// Православный биндинг события
    	$(document).on('uploadDone','#upload_container_dialog', $this.onUploadDone);
    }

    $this.onAddFile = function(){
    	$dialog = $this.createDialog();

    }

    $this.createDialog = function(){
	  	// Получим контейнер
        $container = $("#addFilesHolder");

        // Создаем модальное окно
        $container.modal();

        // Инициируем deffered процесс
        $this.process = $.Deferred();

        // И возвращаем его
        return $this.process;
    }

    $this.onUploadDone = function(){
    	console.log('uploadDone');
    }

    $this.onAddFolder = function(){
    	$this.parent.components.FileListWidget.onAddNewFolder();
    }

    $this.init();
}
