function FileListWidget($selector, $parent){
    var $this = this;

    $this.process = {};


    $this.init = function(){
        $this.selector = $selector;

        // Получаем папки текущего пользователя
        $this.getFolders();

        $this.dropdown = $('#main_dropdown');

        $this.ChangeDirectoryHelper = new ChangeDirectoryHelper('#directoryNavigator', $this);
    }

    $this.getFolders = function(){
        $.ajax({
              type: "POST",
              url: "/folder/list",
              success: function(data){
                $this.getFiles();
                if(!$.isEmptyObject(data)){
                    $.each(data, function($i, $v){

                        // Получаем имя файла из ответа
                        $name = $v.folderName;

                        // Получаем расширение папки
                        $extension = "Папка";

                        // ID папки
                        $fid = $v._id.$id;

                        // Формируем елемент списка с файлом
                        $el = "<tr class='folder' data-type='folder'>";
                        $el += "<td class='extension folder'></td>";
                        $el += "<td class='name'><a href='/folder/change?fid="+$fid+"'>"+$name+"</a><input type='text' style='display:none' class='renameInput'></td>";
                        $el += "<td class='filesize'></td>";
                        $el += "<td class='act'><button class='delete btn btn-danger' act-id = '"+$fid+"'>Удалить</button></td></tr>";

                        // Получим обернутый елемент
                        $li = $($el);

                        // Подписываемся на нажатие кнопки удалить
                        $li.find('.delete').on('click', $this.deleteFolder);

                        // Добавим полученный елемент в список
                        $($this.selector).append($li);

                        $li.on('contextmenu', $this.onContextClick);
                    })
                }
              },
              dataType: "json"
            });
    }

    $this.getFiles = function(){
        $.ajax({
              type: "POST",
              url: "/file/list",
              success: function(data){
                if(!$.isEmptyObject(data)){
                    $.each(data, function($i, $v){

                        var $id = $v._id.$id,
                            $host = $v.host,
                            $uid = $v.user_id;

                        // Получаем имя файла из ответа
                        var $name = $v.user_fileName;

                        // Получаем ссылку на скачиванье файла
                        var $url = "http://" + $host + "/get" + "?uid=" + $uid + "&fid=" + $id;

                        // Получаем расширение файла
                        var $extension = $v.user_fileName.split(".");
                        $extension = $extension[ $extension.length -1 ];

                        // Формируем елемент списка с файлом
                        $el = "<tr class='file' data-type='file'>";
                        $el += "<td class='extension "+$extension+"'></td>";
                        $el += "<td class='name'><a href='"+$url+"'>"+$name+"</a><input type='text' style='display:none' class='renameInput'></td>";
                        $el += "<td class='filesize'>"+$v.human_readable_fs+"</td>";
                        $el += "<td class='act'><button act-id='"+$id+"' class='delete btn btn-danger'>Удалить</button></td></tr>";

                        // Получим обернутый елемент
                        $li = $($el);

                        // Подписываем нажатие кнопки на событие удаление файла
                        $li.find('button').on('click', $this.deleteFile);
                        $li.on('contextmenu', $this.onContextClick);

                        // Добавим полученный елемент в список
                        $($this.selector).append($li);
                    })
                }
              },
              dataType: "json"
            });
    }

    // При клике на елемент
    $this.onContextClick = function(e){
        e.stopPropagation();
        e.preventDefault();
        $this.dropdown
            .data("invokedOn", $(e.target))
            .show()
            .css({
                position: "absolute",
                left: $this.getLeftLocation(e),
                top: $this.getTopLocation(e)
            })
            .off('click')
            .on('click', function (e) {
                $(this).hide();

                e.stopPropagation();
                e.preventDefault();

                var $invokedOn = $(this).data("invokedOn"),
                    $selectedMenu = $(e.target),
                    $act = $selectedMenu.attr('data-act');
                    $row = $invokedOn.closest('tr'),
                    $fid = $row.find('.act button').attr('act-id'),
                    $type = $row.attr('data-type');

                // Получаем тип того, по чему кликнули, с большой буквы
                $type = $type.charAt(0).toUpperCase() + $type.substr(1);

                // Формируем название функции на основе атребута меню
                // Проверяем есть ли такая функция у виджета - если есть
                // вызываем
                // if($.isEmptyObject($this[$act+'File'])){
                    //  Формируем метод, который хотим позвать
                    // и зовем
                    $this[$act + $type].call($this, $invokedOn);
                // }

        });

        //make sure menu closes on any click
        $(document).one('click', function () {
             $this.dropdown.hide();
        });

        return false;
    }

    // Метод перейменования файла
    $this.renameFile = function($el){
        var $row = $el.closest('tr'),
            $fid = $row.find('.act button').attr('act-id');


        $rnResult = $this.showRenameInput($row);

        $rnResult.done(function($data){
             $.ajax({
                  type: "POST",
                  url: "/file/rename",
                  dataType: "json",
                  data : {
                    'id': $fid,
                    'name' : $data.name
                  },
                  success: function($data){
                    // $this.updateFoldersList();
                  }
            });
        });
    }

    // Метод перейменования папки
    $this.renameFolder = function($el){
        var $row = $el.closest('tr'),
            $fid = $row.find('.act button').attr('act-id');


        $rnResult = $this.showRenameInput($row);

        $rnResult.done(function($data){
             $.ajax({
                  type: "POST",
                  url: "/folder/rename",
                  dataType: "json",
                  data : {
                    'id': $fid,
                    'name' : $data.name
                  },
                  success: function($data){
                    // $this.updateFoldersList();
                  }
            });
        });
    }


    $this.hideRenameInput = function($row){
        var $input = $row.find('.renameInput'),
            $link =  $row.find('a'),
            $result = {};

        if($input.val()){
            $input.hide();
            $link.html($input.val()).show();

            $result.name = $input.val();
            $this.process.resolve($result);

        }
        return $row;
    }

    $this.showRenameInput = function($row){
        $this.process = $.Deferred();

        var $input = $row.find('.renameInput'),
            $link =  $row.find('a');

        $input.val($link.html()).show();
        $input.focus();
        $link.hide();

        $input.on('keydown', function($e){
            if($e.keyCode == 13){
                $this.hideRenameInput($row);
            }
        });

        $('html').on('click', function($e){
            if(!$($e.target).hasClass("renameInput") && $input.val()){
                $this.hideRenameInput($row);
                $('html').off('click');
            }
        });

        return $this.process;
    }

    $this.getLeftLocation = function getLeftLocation(e) {
        var mouseWidth = e.pageX;
        var pageWidth = $(window).width();
        var menuWidth = $this.dropdown.width();

        // opening menu would pass the side of the page
        if (mouseWidth + menuWidth > pageWidth &&
            menuWidth < mouseWidth) {
            return mouseWidth - menuWidth;
        }
        return mouseWidth;
    }

    $this.getTopLocation =  function (e) {
        var mouseHeight = e.pageY;
        var pageHeight = $(window).height();
        var menuHeight = $this.dropdown.height();

        // opening menu would pass the bottom of the page
        if (mouseHeight + menuHeight > pageHeight &&
            menuHeight < mouseHeight) {
            return mouseHeight - menuHeight;
        }
        return mouseHeight;
    }

    $this.deleteFolder = function($e){
        // Создаем модальное окно
        $rm = $this.createAllert($($e.currentTarget).parent().parent());

        $fid = $($e.currentTarget).attr('act-id');

        //Если пользователь подтвердил действие
        $rm.done(function(){

             $.ajax({
                  type: "POST",
                  url: "/folder/delete",
                  dataType: "json",
                  data : {
                    'fid': $fid
                  },
                  success: function(){
                    $this.updateFoldersList();
                  }
            });

        });
    }

    $this.deleteFile = function($e){
        // Создаем модальное окно
        $rm = $this.createAllert($($e.currentTarget).parent().parent());

        // Получаем ID файла
        $id = $($e.currentTarget).attr('act-id');

        //Если пользователь подтвердил действие
        $rm.done(function(){

             $.ajax({
                  type: "POST",
                  url: "/file/delete",
                  dataType: "json",
                  data : {
                    'id': $id
                  },
                  success: function(){
                    $this.updateFoldersList();
                  }
            });

        });
    }


    // Метод создания модального окна
    $this.createAllert = function($rmElement){
        // Получим контейнер
        $container = $("#messageHolder");
        // Получим имя удаляемой сущьности
        $name = $rmElement.find('.name').html();

        // Заполним модальное окно
        $container.find('.modal-header h3').html("Подтвердите удаление");
        $container.find('.modal-body p').html("Вы собераетесь удалить " +$name+ "?");

        // Подпишимся на события
        $container.find('#cancel').on('click', $this.onCancel);
        $container.find('#ok').on('click', $this.onOk);

        // Создаем модальное окно
        $container.modal();

        // Инициируем deffered процесс
        $this.process = $.Deferred();

        // И возвращаем его
        return $this.process;
    }

    // При нажатии кнопки отмена в модальном окне
    $this.onCancel = function($e){
        $this.process.fail();

        $container = $($e.currentTarget).parent().parent();
        $container.modal('hide');
    }

    // При нажатии кнопки ok в модальном окне
    $this.onOk = function($e){
        $this.process.resolve();

        $container = $($e.currentTarget).parent().parent();
        $container.modal('hide');
    }

    // Метод вызывающийся по нажатию кнопки создать папку
    $this.onAddNewFolder = function(){
        // Получим контейнер
        $container = $("#newFolderDialog");

        // Cоздаем окно диалога
        $result = $this.createNewFolderDialog($container);

        // Реакция на нажатие пользователем кнопки ок в диалоговом окне создания папки
        $result.done(function(){
            $folderName = $container.find('#folderName').val();

            // Если имя папки не было введенно
            if(!$folderName){
                // Выводим сообщение об ошибке и выходим из метода
                $.showMessage('Не введенно имя папки', $.Registry.ErrorTypes.error);
                return;
            }

            $.ajax({
                  type: "POST",
                  url: "/folder/add",
                  dataType: "json",
                  data : {
                    'name': $folderName
                  },
                  success : function(){

                    // Обновляем список файлов и папок
                    $this.updateFoldersList()
                  }
            });
        });

    }

    // Метод обновления списка папок
    $this.updateFoldersList = function(){
        // Очищаем список файлов и папок
        $($this.selector).find('tbody').html('');

        // Загружаем обновленны файлов и папок
        $this.getFolders();
    }

    $this.createNewFolderDialog = function($container){
        // Инициируем deffered процесс
        $this.process = $.Deferred();

        // Чистим значение инпута с именем папки от предидущих значений
        $container.find("input").val('');

        $container.find("#cancel").on('click', $this.onCancel);
        $container.find("#ok").on('click', $this.onOk);

        // Создаем модальное окно
        $container.modal();

        return $this.process;
    }

    $this.init();
}
