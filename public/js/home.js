$(function(){

    $this = this;
    $this.components = {};

    //	Виджет загрузки файлов
	$this.components.fileUploadWidget = new fileUploadWidget('#upload_container', $this);

	// Виджет, управляющий списком папок и файлов
    $this.components.FileListWidget = new FileListWidget("#files", $this);

    // Виджет управляющий меню пользователя
    $this.components.NavBarWidget = new NavBarWidget("nav", $this);

    // Виджет управляющий действиями пользователя
    $this.components.UserActionWidget = new UserActionWidget("#user_actions", $this);
})
