$.Registry = {
	ErrorTypes: {
		info : 'info',
		success: 'success',
		error: 'error',
		alert: ''
	},
	ErrorTypesRU: {
		info: 'Информация',
		success: 'Успех',
		error: 'Ошибка',
		alert: 'Внимание'
	}
};
