// Метод вывода сообщений пользователю
$.showMessage = function($text, $type){

	// Определим тип сообщения
	$type = ! $type ? '' : $type;

	// Составим класс который нужно подставить
	$className = "alert-" + $type;

	// Получим перевод ошибки на русский
	switch ($type) {
		case 'info' :
			$type_ru = $.Registry.ErrorTypesRU.info;
			break;
		case 'success' :
			$type_ru = $.Registry.ErrorTypesRU.success;
			break;
		case 'error' :
			$type_ru = $.Registry.ErrorTypesRU.error;
			break;
		default :
			$type_ru = $.Registry.ErrorTypesRU.alert;
	}


	$html = "<div class='alert " + $className + "'>";
	$html += "<button type='button' class='close' data-dismiss='alert'>×</button>";
	$html += "<strong>" + $type_ru + ": </strong>"+$text+"<div>";

	console.log('sss');
	$(".wrapper").prepend($html);
}
