$(function() {
	$form = $('form');

	$formWidget = new formWidget($form)

});

function formWidget($form){
	$this = this;
	$this.widget = {};

	$this.init = function(){
		$this.widget = $form;

		$this.widget.find('.uname input').on('focus', $this.onFocus);
		$this.widget.find('.passwd input').on('focus', $this.onFocus);

		$this.widget.find('.uname input').on('input', $this.onKeydown);
		$this.widget.find('.passwd input').on('input', $this.onKeydown);
	}

	$this.onFocus = function($e){
		$this.widget.find('.focused').removeClass('focused');

		$label = $($e.target).parent().find('label');
		$label.addClass('focused');
	}

	$this.onKeydown = function($e){
		$input = $($e.target);

		// Плейсхолдер для инпута
		$label = $($e.target).parent().find('label');

		if($input.val()){
			$label.css('display','none');

			if($this.checkFields()){
				$this.unblockButton();
			}
		}else{
			$label.css('display','inline');
			$this.blockButton();
		}
	}

	// Функция блокирует кнопку отправки формы
	$this.blockButton = function(){
		$this.widget.find('.button input').attr('disabled', 'true');
	}

	// Функция разблокирует функцию отправки формы
	$this.unblockButton = function(){
		$this.widget.find('.button input').removeAttr('disabled');
	}

	// Функция проверяет введенны ли в поля значения
	$this.checkFields = function(){

		// Получим проверяемые поля
		$loginField = $this.widget.find('.uname input');
		$passwordField = $this.widget.find('.passwd input');

		if($loginField.val() && $passwordField.val()){
			return true;
		}else{
			return false;
		}
	}

	$this.init();
}
